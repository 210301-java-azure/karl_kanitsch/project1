window.addEventListener("load",populateEmployeeTable);

function populateEmployeeTable(){
    document.getElementById("employee-table").innerText = "";
    sendAjaxGetRecordsRequest(backendAddr+"/employees",employeeGetSuccess,employeeGetFailure);
}

function employeeGetSuccess(xhr){
    const employees = JSON.parse(xhr.response);
    for(let employee of employees){
        addEmployeeRow(employee);
    }
}

function employeeGetFailure(xhr){
    const body = xhr.response;
    console.log(body);
}

function addEmployeeRow(employee){
    const table = document.getElementById("employee-table");
    let row = document.createElement("tr");
    let eid = employee.employeeID;
    row.addEventListener("click",()=>showEmployeeDetails(eid));

    let id = document.createElement("td");
    id.innerText = employee.employeeID;
    row.appendChild(id);
    
    let firstName = document.createElement("td");
    firstName.innerText = employee.firstName;
    row.appendChild(firstName);
    
    let lastName = document.createElement("td");
    lastName.innerText = employee.lastName;
    row.appendChild(lastName);
    
    let  payRate= document.createElement("td");
    payRate.innerText = employee.payRate;
    row.appendChild(payRate);

    let  accruedPTO= document.createElement("td");
    accruedPTO.innerText = employee.accruedPTO;
    row.appendChild(accruedPTO);
    
    let active = document.createElement("td");
    if(employee.active)
        active.innerText="✔️";
    else
        active.innerText = "❌";
    row.appendChild(active);
    
    let hireDate = document.createElement("td");
    hireDate.innerText = employee.hireDate;
    row.appendChild(hireDate);

    let terminationDate = document.createElement("td");
    terminationDate.innerText = employee.terminationDate;
    row.appendChild(terminationDate);

    let federalID = document.createElement("td");
    let fid = employee.federalID;
    federalID.innerText = "XXX-XX-"+fid.slice(7,11);
    row.appendChild(federalID);

    table.appendChild(row);
}

function showEmployeeDetails(eid){
    const table = document.getElementById("employee-overview");
    table.hidden = true;
    sendAjaxGetRecordsRequest(backendAddr+"/employees/"+eid, populateEmployeeDetail, showGetEmployeeError);
}

function hideEmployeeDetails() {
    const detailCard = document.getElementById("employee-detail");
    detailCard.hidden = true;
    const table = document.getElementById("employee-overview");
    table.hidden = false;
    populateEmployeeTable();
}

function populateEmployeeDetail(xhr){
    const detailCard = document.getElementById("employee-detail");
    detailCard.hidden = false;
    document.getElementsByClassName("close-button")[0].addEventListener("click",hideEmployeeDetails);
    document.getElementById("update-employee").addEventListener("click",updateEmployee);
    document.getElementById("terminate-employee").addEventListener("click",terminateEmployee);
    let employee = JSON.parse(xhr.response);
    document.getElementById("employee-name").innerText=employee.firstName+" "+employee.lastName;
    document.getElementById("eid").value=employee.employeeID;
    document.getElementById("firstName").value=employee.firstName;
    document.getElementById("lastName").value=employee.lastName;
    document.getElementById("payRate").value=employee.payRate;
    document.getElementById("accruedPTO").value=employee.accruedPTO;
    document.getElementById("hireDate").value=employee.hireDate;
    if(employee.active)
        document.getElementById("active").value="✔️";
    else
        document.getElementById("active").value="❌";
    document.getElementById("terminationDate").value=employee.terminationDate;
    document.getElementById("fid").value="XXX-XX-"+employee.federalID.slice(7,11);
    document.getElementById("fid-label").addEventListener("click",()=>{
        if(document.getElementById("fid").value.slice(0,1)==="X"){
            document.getElementById("fid").value = employee.federalID;
        } else {
            document.getElementById("fid").value = "XXX-XX-"+employee.federalID.slice(7,11);
        }
    });
}

function showGetEmployeeError(xhr){
    console.log("Error Encountered in AJAX get")
}


function updateEmployee(event){
    event.preventDefault();
    let eid = document.getElementById("eid").value;
    console.log("Updating employee with ID: "+eid);
    formData = {
        "operation":"update",
        "employeeID":document.getElementById("eid").value,
        "firstName":document.getElementById("firstName").value,
        "lastName":document.getElementById("lastName").value,
        "payRate":document.getElementById("payRate").value,
        "accruedPTO":document.getElementById("accruedPTO").value
    }
    sendAjaxUpdateEmployeeRequest(backendAddr+"/employees/"+eid,formData,employeeUpdateSuccess,employeeUpdateFailure);
}

function terminateEmployee(event){
    event.preventDefault();
    let eid = document.getElementById("eid").value;
    console.log("Terminating employee with ID: "+eid);
    sendAjaxTerminateEmployeeRequest(backendAddr+"/employees/"+eid,eid,employeeTerminationSuccess,employeeTerminationFailure)
}

function employeeUpdateSuccess(){
    let statusMessage = document.getElementById("status-message");
    statusMessage.innerText = "Employee Updated Successfully!";
    hideEmployeeDetails();
    displayGreen(statusMessage,5000);
}

function employeeUpdateFailure(){
    let statusMessage = document.getElementById("status-message");
    statusMessage.innerText = "Error encountered, Employee not updated";
    displayRed(statusMessage,5000);
}

function employeeTerminationSuccess(){
    let statusMessage = document.getElementById("status-message");
    statusMessage.innerText = "Employee Terminated Successfully!";
    displayGreen(statusMessage,5000);
}

function employeeTerminationFailure(){
    let statusMessage = document.getElementById("status-message");
    statusMessage.innerText = "Error encountered, Employee not terminated";
    displayRed(statusMessage,5000);
}

function displayGreen(domElement,timeout){
    domElement.classList.add("status-message-green");
    domElement.classList.remove("invisible");
    setTimeout(hideStatusMessage, timeout);
}

function displayRed(domElement,timeout){
    domElement.classList.add("status-message-red");
    domElement.classList.remove("invisible");
    setTimeout(hideStatusMessage, timeout);
}

function hideStatusMessage(){
    document.getElementById("status-message").classList.add("invisible");
    document.getElementById("status-message").classList.remove("status-message-green");
    document.getElementById("status-message").classList.remove("status-message-red");
}
