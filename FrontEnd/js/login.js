window.addEventListener("load", function() {
    if(!checkAuth())
        document.getElementById("login-form").addEventListener("submit",authenticateUser);
});

function loginSuccess(xhr){
    const authToken = xhr.getResponseHeader("auth-token");
    sessionStorage.setItem("auth-token", authToken);
    location.href="exposhrfrontend/index.html";
}

function loginFailure(xhr){
    console.log("Login failed");
    location.reload();
}