let specialChars = [",",".","!","@","#","*"];
let specialCharsString = "[";

window.addEventListener("load", function() {
    document.getElementById("registration-form").addEventListener("submit",registerNewUser);
    document.getElementById("password").addEventListener("blur",checkPasswords);
    document.getElementById("password_confirmation").addEventListener("change",checkPasswords);
    for(let char of specialChars){
        specialCharsString += char + "  ";
    }
    specialCharsString += "]";
    document.getElementById("special_char_warning").innerText="Passwords must contain at least 1 special character: "+specialCharsString;
});

function registerNewUser(event){
    event.preventDefault();
    const username = document.getElementById("username").value;
    const email = document.getElementById("email").value;
    if(document.getElementById("password").value === document.getElementById("password_confirmation").value) {
        const password = document.getElementById("password").value
    }
    sendAjaxRegisterNewUserRequest(username, password, email, registrationSuccess, registrationFailure);
}

function registrationSuccess(){
    console.log("registration success");
    location.href="/login.html";
}

function registrationFailure(){
    console.log("registration failure");
}

function checkPasswords(){
    const password1 = document.getElementById("password").value;
    const password2 = document.getElementById("password_confirmation").value;
    let hasGoodLength = false;
    let hasSpecialChar = false;
    let hasBothCases = false;
    let numberCheck = hasNumbers(password1);
    let passwordsMatch = false;
    if(password1.length >= 8){
        hasGoodLength = true;
    }
    if(hasGoodLength){
        document.getElementById("password_length_warning").hidden = true;
    } else {
        document.getElementById("password_length_warning").hidden = false;
    }

    for(let char in specialChars){
        if(password1.includes(char)){
            hasSpecialChar = true;
        }
    }
    if(hasSpecialChar){
        document.getElementById("special_char_warning").hidden = true;
    } else {
        document.getElementById("special_char_warning").hidden = false;
    }
        
    if(hasLowerCase(password1) && hasUpperCase(password1)){
        hasBothCases = true;
    }
    if(hasBothCases){
        document.getElementById("password_case_warning").hidden = true;
    } else {
        document.getElementById("password_case_warning").hidden = false;
    }

    if(numberCheck) {
        document.getElementById("password_number_warning").hidden = true;
    } else {
        document.getElementById("password_number_warning").hidden = false;
    }

    if(password1===password2){
        passwordsMatch = true;
    }
    if(passwordsMatch){
        document.getElementById("password_match_warning").hidden = true;
    } else {
        document.getElementById("password_match_warning").hidden = false;
    }
    
    if(hasGoodLength && hasSpecialChar && hasBothCases && numberCheck && passwordsMatch){
        document.getElementById("submit_btn").disabled = false;
    }
}

function hasLowerCase(string){
    return string.toUpperCase() != string;
}

function hasUpperCase(string){
    return string.toLowerCase() != string;
}

function hasNumbers(string) {
    return (/[0-9]/.test(string));
}
