window.addEventListener("load",populateEmployeeInfo);

function populateEmployeeInfo(){
    sendAjaxAuthenticateRequest(showEmployeeInfo,showUnauthorizedResponse);
}

function showEmployeeInfo(xhr){
    sendAjaxGetRecordsRequest(backendAddr+"/employee",populateEmployeeDetail,emptyFunction);
}

function parseEmployee(xhr){
    let body = JSON.parse(JSON.parse(xhr.response));
    console.log(body);
}

function showUnauthorizedResponse(){

}

function emptyFunction(){}

function populateEmployeeDetail(xhr){
    const detailCard = document.getElementById("employee-detail");
    let employee = JSON.parse(JSON.parse(xhr.response));
    document.getElementById("employee-name").innerText=employee.firstName+" "+employee.lastName;
    document.getElementById("eid").value=employee.employeeID;
    document.getElementById("firstName").value=employee.firstName;
    document.getElementById("lastName").value=employee.lastName;
    document.getElementById("payRate").value=employee.payRate;
    document.getElementById("accruedPTO").value=employee.accruedPTO;
    document.getElementById("hireDate").value=employee.hireDate;
    if(employee.active)
        document.getElementById("active").value="✔️";
    else
        document.getElementById("active").value="❌";
    document.getElementById("terminationDate").value=employee.terminationDate;
    document.getElementById("fid").value="XXX-XX-"+employee.federalID.slice(7,11);
    document.getElementById("fid-label").addEventListener("click",()=>{
        if(document.getElementById("fid").value.slice(0,1)==="X"){
            document.getElementById("fid").value = employee.federalID;
        } else {
            document.getElementById("fid").value = "XXX-XX-"+employee.federalID.slice(7,11);
        }
    });
}

