# ExposHR Website Design and Functionality

[ExposHR Home](http://karlkanitschrevature.blob.core.windows.net/exposhrfrontend/index.html)

## Basic Information  
Provides a framework for viewing and managing employee information intended to be used by both employees and HR personel.  
This website uses bootstrap as its primary framework, but also incorporates custom CSS coordinated through SASS scss files.  
The navbar and footer elements are dynamically incorporated into each page using fetch, but all HTTP methods are implemented using AJAX.  

## Implemented Pages  
### `/index.html` 
The sites landing page. Currently only includes placeholder text and a single button to register new users, but could be a fully fledged landing page showcasing the web apps abilities.

---

### `/register.html`  
Used to create a new user account. If an account is successfully created, the user is redirected to the login page.

---

### `/login.html`  
Allows users to attempt to login. On a successful login, an authToken is saved to the browsers sessionStorage and the user is redirected to the home page.

---

### `/logout.html`  
Logs the user out and provides a link to log back in. This clears the authToken from sessionStorage.

---

### `/employee.html`  
Allows any logged in user to see their employee information assuming they have an employee id linked to their user account.

---

### `/user.html`  
Allows any logged in user to see information about their user account and provides a link to change the users password.

---

### `/change-password.html`  
Allows any logged in user to change their password. They are then logged out and directed to log back in with their new password.

---

### `/employees.html`  
**HR Access Required**
Allows Human Resource managers to veiw and manipulate employee data, including hiring new employees, terminating employment and editing an employees name, pay rate and accrued paid time off. All other users will see only an empty table, and while they can use the button to attempt to add a new employee through the `/hire` endpoint, without authorization any attempts to do so will fail.  

---

### `/hire.html`  
**HR Access Required**  
Allows adding new employees to the database. While non-HR users can see the page, they can not persist any changes to the database.

---

### `/users.html`  
**Admin Access Required**  
Provides a list of all registered user accounts and allows system administrators to change a user accounts role or linked employee id. Will be empty for all other users.

---

## Known Issues
- Newly registered users sometimes have password matching errors. Changing passwords doesn't seem to create similar problems and the cause is yet unknown.




