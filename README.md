# ExposHR

## Project Description

ExposHr is a human resources management application which allows employees and employers access to relevant information regarding an employee. Employers can add new employees, track hire and fire dates, paid time off and other pertinent details. Employees can view all of this information and make changes to their account, such as changing their password. Password hashing and authentication was handled using Bcrypt for added security and session state was stored remotely on the client machine using signed JWT.

## Technologies Used

- Java 8, including Javalin and Hibernate
- HTML 5, CSS 3, ES6
- Bootstrap 4

## Features

**Implemented Features:**  
- New user registration allows an employee to sign up with the service, creating a private account with password protection.
- Passwords are fully hashed before being stored in the database. Bcrypt is used to both hash the passwords and compare the user entered password to the stored password during authentication.
- New employees can be added to the database by users with HR privileges. The hire date will be set to the current date.
- Employees can have their employment terminated by users with HR privileges.

**To-do list:**  
- We need to add more admin functionality, in particular, the ability to link a user account to an employee.
- Add in timesheets with calculated pay, paid time-off and HR approval.

## Getting Started
   
Use the following git clone command to copy the code to your own machine. This will create a new folder in your current directory with two folders, one for the front end and one for the back end.  
> `git clone https://gitlab.com/210301-java-azure/karl_kanitsch/project1.git`  

**Setting up the backend server**  
Note that you will need the JDK in order to build and run the back end. While I recommend using the v1.8 JDK, I expect it will work fine with some later versions of the JDK.  
You will need to set up environment variables for your database, either in your system or in your IDE. The variables used in `dev.kkanit.utilities.HibernateUtil` are listed below:
> |Variable Name|References|
> |---|---|
> |P1DBURL|The url of the database being used|
> |P1DBUSER|The username for logging into the database|
> |P1DBPASS|The password for logging into the database|
  


If everything is set up correctly, running the java program should print the following to the console.


![](images/Javalin_startup.png)


**Setting up the frontend server**  
The frontend needs to run on a server to display properly. While there are a lot of ways to do this, including express or blob storage through a cloud hosting service, for testing you can use something like [Live Server for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer). Once it is running correctly, you should a page which resembles the following picture, with a navbar and footer.

![](images/Exposhr_home.png)

## Usage

1. To begin using the program, you will need to connect to a database. The Hibernate HBM2DDL_AUTO property is set to `update` by default, so it should create the tables for you. 
2. You will then need to register an account and grant it HR privileges using a SQL interface.  
> `UPDATE user_account ua SET role_id = 2 WHERE ua.user_id = 1;`
3. From here you can start to hire new employees and work with the employee database. If you wish to create an admin account to look at the user database, register a new account and set the role to 3.
4. To see an employee view of the application, update a registered user account to reference an existing employee record in SQL.  
> `UPDATE user_account ua SET employee_id = <valid_employee_id> WHERE ua.user_id = <user_id>;`  

This will allow you to work with the implemented funcitonality of the application, though you will need to populate your database and make certain adjustments in it manually. Some SQL scripts that I used during development and testing are included in the `BackEnd/SQLFiles/` folder.