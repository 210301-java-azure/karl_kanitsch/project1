package dev.kkanit.services;

import dev.kkanit.dao.UserDAO;
import dev.kkanit.models.UserAccount;
import io.javalin.http.BadRequestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Mock
    UserDAO uDAO;

    @Mock
    AuthService authService;

    @InjectMocks
    UserService userService;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private UserAccount getTestUser(){
        UserAccount testUser = new UserAccount();
        testUser.setUsername("testUser");
        testUser.setPassword("testPassword");
        testUser.setEmail("test@email.com");
        return testUser;
    }

//    @Disabled
    @Test
    public void testRegisterNewUserSuccess(){
        UserAccount testUser = getTestUser();
        List<String> emptyList = new ArrayList<>();
        when(uDAO.getAllUsernames()).thenReturn(emptyList);
        when(uDAO.getAllEmails()).thenReturn(emptyList);
        when(authService.hashPassword("testPassword")).thenReturn("testPassword");
        when(uDAO.addNewUser(testUser.getUsername(), testUser.getPassword(), testUser.getEmail())).thenReturn(testUser);
        assertEquals(testUser,userService.registerNewUser(testUser.getUsername(), testUser.getPassword(), testUser.getEmail()));
    }

//    @Disabled
    @Test
    public void testRegisterNewUserFailureUsername(){
        UserAccount testUser = getTestUser();
        List<String> emptyList = new ArrayList<>();
        emptyList.add("testUser");
        when(uDAO.getAllUsernames()).thenReturn(emptyList);
        when(uDAO.getAllEmails()).thenReturn(emptyList);
        when(uDAO.addNewUser(testUser.getUsername(), testUser.getPassword(), testUser.getEmail())).thenReturn(testUser);
        assertThrows(BadRequestResponse.class,()->userService.registerNewUser(testUser.getUsername(),
                testUser.getPassword(), testUser.getEmail()));
    }

//    @Disabled
    @Test
    public void testRegisterNewUserFailureEmail(){
        UserAccount testUser = getTestUser();
        List<String> emptyList = new ArrayList<>();
        emptyList.add("test@email.com");
        when(uDAO.getAllUsernames()).thenReturn(emptyList);
        when(uDAO.getAllEmails()).thenReturn(emptyList);
        when(uDAO.addNewUser(testUser.getUsername(), testUser.getPassword(), testUser.getEmail())).thenReturn(testUser);
        assertThrows(BadRequestResponse.class,()->userService.registerNewUser(testUser.getUsername(),
                testUser.getPassword(), testUser.getEmail()));
    }

//    @Disabled
    @Test
    public void testChangeUserPasswordSuccess(){
        UserAccount testUser = getTestUser();
        testUser.setUserID(-1);
        when(uDAO.getUserAccount("testUser")).thenReturn(testUser);
        when(uDAO.changeUserPassword(-1,"newPassword")).thenReturn(true);
        assertTrue(userService.changeUserPassword("testUser","newPassword"));
    }

    @Test
    public void testGetEmployeeID(){

    }
}
