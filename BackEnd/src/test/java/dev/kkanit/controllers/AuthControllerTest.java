package dev.kkanit.controllers;

import dev.kkanit.services.AuthService;
import dev.kkanit.services.UserService;
import io.javalin.http.UnauthorizedResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.javalin.http.Context;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AuthControllerTest {

    @Mock
    private AuthService authService;

    @Mock
    private UserService userService;

    @InjectMocks
    private AuthController authController;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private static final Context context = mock(Context.class);

    @Test
    public void testHandleAuthenticateUserSuccess() {
        when(context.formParam("username")).thenReturn("username");
        when(context.formParam("password")).thenReturn("password");
        when(authService.authenticate("username","password")).thenReturn(1);
        when(userService.getUserID("username")).thenReturn(1);
        when(authService.generateAuthToken("username", 1, 1)).thenReturn("Fake JWT returned.");
        authController.handleAuthenticateUser(context);
        verify(context).header("auth-token","Fake JWT returned.");
    }

    @Test
    public void testHandleAuthenticateUserNullUsernamePassword() {
        when(context.formParam("username")).thenReturn(null);
        when(context.formParam("password")).thenReturn(null);
        assertThrows(UnauthorizedResponse.class, () -> authController.handleAuthenticateUser(context));
    }

    @Test
    public void testHandleAuthenticateUserInvalidPassword() {
        when(context.formParam("username")).thenReturn("username");
        when(context.formParam("password")).thenReturn("wrongpassword");
        assertThrows(UnauthorizedResponse.class,()->authController.handleAuthenticateUser(context));
    }

    @Test
    public void testHandleLoginGet(){
        Map<String,String> headerMap = mock(Map.class);
        Map<String, Object> claimsMap = new HashMap<>();
        claimsMap.put("username","FakeUser");
        claimsMap.put("roleID",2);
        when(context.headerMap()).thenReturn(headerMap);
        when(headerMap.containsKey("auth-token")).thenReturn(true);
        when(headerMap.get("auth-token")).thenReturn("Fake JWT");
        when(authService.validateAuthToken("Fake JWT")).thenReturn(claimsMap);
        authController.handleLoginGet(context);
        verify(context).result("Welcome, FakeUser\nYou currently have roleID: 2");
    }

    @Test
    public void testGetUsernameFromToken(){
        Map<String,String> headerMap = mock(Map.class);
        when(context.headerMap()).thenReturn(headerMap);
        when(headerMap.containsKey("auth-token")).thenReturn(true);
        when(headerMap.get("auth-token")).thenReturn("Fake JWT");
        when(authService.getUsernameFromToken("Fake JWT")).thenReturn("FakeUser");
        assertEquals("FakeUser",authController.getUsernameFromToken(context));
    }

    @Test
    public void testGetRoleIDFromToken(){
        Map<String,String> headerMap = mock(Map.class);
        when(context.headerMap()).thenReturn(headerMap);
        when(headerMap.containsKey("auth-token")).thenReturn(true);
        when(headerMap.get("auth-token")).thenReturn("Fake JWT");
        when(authService.getRoleIDFromToken("Fake JWT")).thenReturn(414);
        assertEquals(414,authController.getRoleIDFromToken(context));
    }

    @Test
    public void testSanitizeJWT(){
        assertAll(
                ()->assertEquals("Fake JWT",authController.sanitizeJWT("Fake JWT")),
                ()->assertEquals("Fake JWT",authController.sanitizeJWT("\"\"\"Fake JWT\""))
        );
    }
}
