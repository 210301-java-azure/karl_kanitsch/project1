package dev.kkanit.controllers;

import dev.kkanit.models.UserAccount;
import dev.kkanit.services.AuthService;
import dev.kkanit.services.UserService;
import io.javalin.http.BadRequestResponse;
import org.junit.jupiter.api.BeforeEach;

import io.javalin.http.Context;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private AuthService authService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private static final Logger logger = LoggerFactory.getLogger(UserControllerTest.class);
    private static final Context context = mock(Context.class);

    @Test
    public void testHandleNewUserUnknownFailure(){
        Map paramMap = new HashMap();
        when(context.formParamMap()).thenReturn(paramMap);
        when(context.formParam("username")).thenReturn("fakeUser");
        when(context.formParam("password")).thenReturn("fakePassword");
        when(context.formParam("email")).thenReturn("fake@email.com");
        when(userService.registerNewUser("fakeUser", "fakePassword", "fake@email.com")).thenReturn(new UserAccount());
        assertThrows(BadRequestResponse.class,()->userController.handleNewUser(context));
    }

    @Test
    public void testHandleNewUserSuccess(){
        Map paramMap = new HashMap();
        UserAccount userAccount = new UserAccount("fakeUser","fakePassword",1,"fake@email.com",1);
        userAccount.setUserID(-1);
        when(context.formParamMap()).thenReturn(paramMap);
        when(context.formParam("username")).thenReturn("fakeUser");
        when(context.formParam("password")).thenReturn("fakePassword");
        when(context.formParam("email")).thenReturn("fake@email.com");
        when(userService.registerNewUser("fakeUser", "fakePassword", "fake@email.com")).thenReturn(userAccount);
        userController.handleNewUser(context);
        verify(context).header("username","fakeUser");
    }

//
//    @Test
//    public void testHandleChangePasswordFailure(){
//        Map paramMap = new HashMap();
//        when(context.formParamMap()).thenReturn(paramMap);
//        when(context.formParam("username")).thenReturn("username");
//        when(context.formParam("password")).thenReturn("old-password");
//        when(context.formParam("new-password")).thenReturn("new-password");
//        when(userService.changeUserPassword("username","old-password","new-password")).thenReturn(0);
//        assertThrows(BadRequestResponse.class,()->userController.handleChangePassword(context));
//    }
//
//    @Test
//    public void testHandleChangePasswordSuccess(){
//        Map paramMap = new HashMap();
//        when(context.formParamMap()).thenReturn(paramMap);
//        when(context.formParam("username")).thenReturn("username");
//        when(context.formParam("password")).thenReturn("password");
//        when(context.formParam("new-password")).thenReturn("new-password");
//        when(authService.authenticate("username","password")).thenReturn(1);
//        when(userService.changeUserPassword("username","new-password")).thenReturn(true);
//        userController.handleChangeUserPassword(context);
//        verify(context).status(200);
//    }
//
//    @Test
//    public void testHandleChangePasswordUnauthorized(){
//        Map paramMap = new HashMap();
//        when(context.formParamMap()).thenReturn(paramMap);
//        when(context.formParam("username")).thenReturn("username");
//        when(context.formParam("password")).thenReturn("wrongPassword");
//        when(context.formParam("new-password")).thenReturn("new-password");
//        when(authService.authenticate("username","password")).thenReturn(1);
//        when(userService.changeUserPassword("username","new-password")).thenReturn(true);
//        assertThrows(BadRequestResponse.class,()->userController.handleChangeUserPassword(context));
//    }
//
//    @Test
//    public void testHandleChangePasswordInternalError(){
//        Map paramMap = new HashMap();
//        when(context.formParamMap()).thenReturn(paramMap);
//        when(context.formParam("username")).thenReturn("username");
//        when(context.formParam("password")).thenReturn("password");
//        when(context.formParam("new-password")).thenReturn("new-password");
//        when(authService.authenticate("username","password")).thenReturn(1);
//        when(userService.changeUserPassword("username","new-password")).thenReturn(false);
//        assertThrows(BadRequestResponse.class,()->userController.handleChangeUserPassword(context));
////        assertEquals(500,context.status());
//    }
//
//    @Test
//    public void testHandleGetUserRequestUnauthorized(){
//
//    }
//
//    @Test
//    public void testHandleGetUserRequestAuthorized(){
//
//    }
//
//    @Test
//    public void testHandleGetUserRequestHR(){
//
//    }
}
