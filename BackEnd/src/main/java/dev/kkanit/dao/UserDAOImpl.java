package dev.kkanit.dao;

import dev.kkanit.models.UserAccount;
import dev.kkanit.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;

public class UserDAOImpl implements UserDAO {

    Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

    @Override
    public UserAccount addNewUser(String username, String password, String email) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            UserAccount user = new UserAccount(username, password, 1, email, 0);
            int userID = (int) s.save(user);
            user.setUserID(userID);
            tx.commit();
            return user;
        }
    }

    @Override
    public List<String> getAllUsernames() {
        try(Session s = HibernateUtil.getSession()){
            List<String> usernames = s.createQuery("Select username from UserAccount").list();
            return usernames;
        }
    }

    @Override
    public List<String> getAllEmails() {
        try(Session s = HibernateUtil.getSession()){
            List<String> emails = s.createQuery("Select email from UserAccount").list();
            return emails;
        }
    }

    @Override
    public UserAccount getUserAccount(String username) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            cq.select(root.get("userID"));
            cq.where(cb.equal(root.get("username"),username));
            if(s.createQuery(cq).uniqueResult()==null)
                return null;
            int userID = s.createQuery(cq).uniqueResult();
            return s.get(UserAccount.class,userID);
        }
    }

    @Override
    public UserAccount getUserAccount(int userID) {
        try(Session s = HibernateUtil.getSession()){
            return s.get(UserAccount.class,userID);
        }
    }

    @Override
    public List<UserAccount> getAllUserAccounts() {
        try(Session s = HibernateUtil.getSession()){
            return s.createQuery("FROM UserAccount").list();
        }
    }

    @Override
    public List<UserAccount> getFilteredUserAccounts(Map<String, String> queryMap) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<UserAccount> cq = cb.createQuery(UserAccount.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            for(String key: queryMap.keySet()){
                if(queryMap.get(key).contains("_LIKE_"))
                    cq.where(cb.like(root.get(key),queryMap.get(key).substring(5)));
                else if(queryMap.get(key).contains("_GT_"))
                    cq.where(cb.greaterThan(root.get(key),queryMap.get(key).substring(4)));
                else if(queryMap.get(key).contains("_GTE_"))
                    cq.where(cb.greaterThanOrEqualTo(root.get(key),queryMap.get(key).substring(5)));
                else if(queryMap.get(key).contains("_LT_"))
                    cq.where(cb.lessThan(root.get(key),queryMap.get(key).substring(4)));
                else if(queryMap.get(key).contains("_LTE_"))
                    cq.where(cb.lessThanOrEqualTo(root.get(key),queryMap.get(key).substring(5)));
                else
                    cq.where(cb.equal(root.get(key),queryMap.get(key)));
            }
            return s.createQuery(cq).list();
        }
    }

    @Override
    public String getUserPassword(String username) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<String> cq = cb.createQuery(String.class);
            Root<UserAccount> user = cq.from(UserAccount.class);
            cq.select(user.get("password"));
            cq.where(cb.equal(user.get("username"),username));
            return s.createQuery(cq).uniqueResult();
        }
    }

    @Override
    public boolean changeUserPassword(int userID, String password) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<UserAccount> cq = cb.createQuery(UserAccount.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            cq.where(cb.equal(root.get("userID"),userID));
            if(s.createQuery(cq).uniqueResult()==null)
                return false;
            UserAccount userAccount = s.createQuery(cq).uniqueResult();
            userAccount.setPassword(password);
            UserAccount updatedAccount = (UserAccount) s.merge(userAccount);
            tx.commit();
            return updatedAccount.getPassword() == password;
        }
    }

    @Override
    public boolean setRole(int userID, int roleID) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<UserAccount> cq = cb.createQuery(UserAccount.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            cq.where(cb.equal(root.get("userID"),userID));
            if(s.createQuery(cq).uniqueResult()==null)
                return false;
            UserAccount userAccount = s.createQuery(cq).uniqueResult();
            userAccount.setRoleID(roleID);
            UserAccount updatedAccount = (UserAccount) s.merge(userAccount);
            tx.commit();
            return updatedAccount.getRoleID() == roleID;
        }
    }

    @Override
    public int getRole(String username) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            cq.select(root.get("roleID"));
            cq.where(cb.equal(root.get("username"),username));
            int roleID = s.createQuery(cq).uniqueResult();
            System.out.println("uDAO getRole roleID: " + roleID);
            return roleID;
        }
    }

    @Override
    public boolean setEmployeeID(int userID, int employeeID) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<UserAccount> cq = cb.createQuery(UserAccount.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            cq.where(cb.equal(root.get("userID"),userID));
            if(s.createQuery(cq).uniqueResult()==null)
                return false;
            UserAccount userAccount = s.createQuery(cq).uniqueResult();
            userAccount.setEmployeeID(employeeID);
            UserAccount updatedAccount = (UserAccount) s.merge(userAccount);
            tx.commit();
            return updatedAccount.getEmployeeID() == employeeID;
        }
    }

    @Override
    public int getEmployeeID(String username) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<UserAccount> root = cq.from(UserAccount.class);
            cq.select(root.get("employeeID"));
            cq.where(cb.equal(root.get("username"),username));
            return s.createQuery(cq).uniqueResult();
        }
    }

    @Override
    public boolean updateUser(UserAccount user) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.merge(user);
            tx.commit();
        }
        return user.equals(getUserAccount(user.getUserID()));
    }

    @Override
    public boolean deleteUser(UserAccount user) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(user);
            tx.commit();
            return true;
        }
    }

    //Utility class which populates the database with fake user accounts read in from a csv in the SQLFiles folder.
    @Override
    public void populateDB(String username, String password, int roleID, String email, int employeeID) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            UserAccount user = new UserAccount(username, password, roleID, email, employeeID);
            int userID = (int) s.save(user);
            tx.commit();
//            user.setUserID(userID);
//            logger.info(user.toString());
        }
    }
}
