package dev.kkanit.dao;

import dev.kkanit.models.Employee;

import java.util.List;

public interface EmployeeDAO {

    List<Employee> getAllEmployees();
    Employee getEmployee(int employeeID);
    Employee getEmployeeByFID(String federalID);
    int addEmployee(String firstName, String lastName, double payRate, String federalID);
    boolean removeEmployee(int employeeID);
    void updateEmployee(Employee employee);
    void populateDB(String firstName, String lastName, String hireDate, String terminationDate, boolean active,
                    double payRate, double accruedPTO, String federalID);
}
