package dev.kkanit.dao;

import dev.kkanit.models.Role;
import dev.kkanit.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class RoleDAOImpl implements RoleDAO {
    @Override
    public Role addRole(Role role) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int roleID = (int) s.save(role);
            role.setRoleID(roleID);
            tx.commit();
            return role;
        }
    }

    @Override
    public String getRoleName(int roleID) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<String> cq = cb.createQuery(String.class);
            Root<Role> root = cq.from(Role.class);
            cq.select(root.get("roleName"));
            cq.where(cb.equal(root.get("roleID"),roleID));
            String roleName = s.createQuery(cq).uniqueResult();
            return(roleName);
        }
    }

    @Override
    public boolean hasUserAccess(int roleID) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Boolean> cq = cb.createQuery(Boolean.class);
            Root<Role> root = cq.from(Role.class);
            cq.select(root.get("hasUserAccess"));
            cq.where(cb.equal(root.get("roleID"),roleID));
            boolean hasAccess = s.createQuery(cq).uniqueResult();
            System.out.println(hasAccess);
            return hasAccess;
        } catch(NullPointerException npe) {
            return false;
        }
    }

    @Override
    public boolean hasHRAccess(int roleID) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Boolean> cq = cb.createQuery(Boolean.class);
            Root<Role> root = cq.from(Role.class);
            cq.select(root.get("hasHRAccess"));
            cq.where(cb.equal(root.get("roleID"),roleID));
            return s.createQuery(cq).uniqueResult();
        } catch(NullPointerException npe) {
            return false;
        }
    }

    @Override
    public boolean hasAdminAccess(int roleID) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Boolean> cq = cb.createQuery(Boolean.class);
            Root<Role> root = cq.from(Role.class);
            cq.select(root.get("hasAdminAccess"));
            cq.where(cb.equal(root.get("roleID"),roleID));
            return s.createQuery(cq).uniqueResult();
        } catch(NullPointerException npe) {
            return false;
        }
    }

    @Override
    public boolean deleteRole(Role role) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(role);
            tx.commit();
            return getRoleName(role.getRoleID())==null ? true : false;
        }
    }
}
