package dev.kkanit.dao;

import dev.kkanit.models.Employee;
import dev.kkanit.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {

    Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);

    @Override
    public List<Employee> getAllEmployees() {
        try(Session s = HibernateUtil.getSession()){
            List<Employee> employees = s.createQuery("FROM Employee").list();
            return employees;
        }
    }

    @Override
    public Employee getEmployee(int employeeID) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
            Root<Employee> employeeRoot = cq.from(Employee.class);
            cq.where(cb.equal(employeeRoot.get("employeeID"),employeeID));
            return s.createQuery(cq).uniqueResult();
        }
    }

    @Override
    public Employee getEmployeeByFID(String federalID){
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
            Root<Employee> root = cq.from(Employee.class);
            cq.where(cb.equal(root.get("federalID"),federalID));
            return s.createQuery(cq).uniqueResult();
        }
    }

    @Override
    public int addEmployee(String firstName, String lastName, double payRate, String federalID) {
        Employee employee = new Employee(firstName, lastName, payRate, federalID);
        employee.setHireDate(LocalDate.now().toString());
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int employeeID = (int) s.save(employee);
            tx.commit();
            return employeeID;
        }
    }

    @Override
    public boolean removeEmployee(int employeeID) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
            Root<Employee> root = cq.from(Employee.class);
            cq.where(cb.equal(root.get("employeeID"),employeeID));
            if(s.createQuery(cq).uniqueResult()==null)
                return false;
            Employee employee = s.createQuery(cq).uniqueResult();
            s.delete(employee);
            tx.commit();
            return true;
        }
    }

    @Override
    public void updateEmployee(Employee employee) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.merge(employee);
            tx.commit();
        }
    }

    @Override
    public void populateDB(String firstName, String lastName, String hireDate, String terminationDate, boolean active, double payRate, double accruedPTO, String federalID) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Employee employee = new Employee(firstName,lastName,hireDate,terminationDate,active,payRate,accruedPTO,
                    federalID);
            int employeeID = (int) s.save(employee);
            tx.commit();
//            logger.info("EmployeeID: "+employeeID+" \tFirst Name: "+firstName+" \tLast Name: "+lastName);
        }
    }
}
