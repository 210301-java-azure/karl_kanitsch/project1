package dev.kkanit.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kkanit.models.UserAccount;
import dev.kkanit.services.AuthService;
import dev.kkanit.services.UserService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserController {

    private UserService userService = new UserService();
    private AuthService authService = new AuthService();
    private AuthController authController = new AuthController();
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    public void handleNewUser(Context ctx) throws BadRequestResponse {
        if (ctx.formParamMap()==null || ctx.formParam("username")==null || ctx.formParam("password")==null || ctx.formParam("email")==null)
            throw new BadRequestResponse("Please fill in all required fields and try again.");
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        String email = ctx.formParam("email");
        UserAccount user = userService.registerNewUser(username, password, email);
        if(user.getUserID() == 0)
            throw new BadRequestResponse("There was a problem creating the new user account. If the problem persists," +
                    " please contact a system administrator.");
        ctx.status(201);
        ctx.header("username",username);
    }

    public void handleChangeUserPassword(Context ctx){
        if (ctx.formParamMap()==null || ctx.formParam("username")==null || ctx.formParam("password")==null || ctx.formParam("new-password")==null)
            throw new BadRequestResponse("Please fill in all required fields and try again.");
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        String newPassword = ctx.formParam("new-password");
        if(authService.authenticate(username,password) <= 0)
            throw new BadRequestResponse("Invalid login credentials provided.");
        logger.info("Result of userService,changeUserPassword call: "+userService.changeUserPassword(username,
                newPassword));
        if(userService.changeUserPassword(username, newPassword))
            ctx.status(200);
        else {
            logger.info("We are in the else statement.");
            ctx.status(404);
            logger.info("status: "+ctx.status());
        }
    }

    public void handleGetUserRequest(Context ctx) {
        int userID = authController.getUserIDFromToken(ctx);
        int searchID = 0;
        try {
            searchID = Integer.parseInt(ctx.pathParam("id"));
        } catch(NumberFormatException nfe) {
            logger.warn("Could not parse " + ctx.pathParam("id") + " to an integer.");
            throw new BadRequestResponse("Invalid userID provided.");
        }
        if(searchID == userID)
            ctx.json(userService.getUserAccount(authController.getUsernameFromToken(ctx)));
        else if(authController.hasAdminAccess(ctx))
            ctx.json(userService.getUserAccount(searchID));
    }

    public void handleGetAllUsersRequest(Context ctx){
        if(!authController.hasAdminAccess(ctx))
            throw new BadRequestResponse("You don't have permission to view all user accounts");
        ctx.json(userService.getAllUserAccounts());
    }

    public void handlePostUserRequest(Context ctx) {
        int userID, employeeID, roleID;
        if(!authController.hasAdminAccess(ctx))
            throw new UnauthorizedResponse("You are not authorized to edit this data.");
        try {
            userID = Integer.parseInt(ctx.formParamMap().get("userID").get(0));
        } catch (NumberFormatException nfe) {
            throw new BadRequestResponse("Unable to parse userID: "+ ctx.formParamMap().get("userID").get(0) + " into an int.");
        }
        try {
            employeeID = Integer.parseInt(ctx.formParamMap().get("employeeID").get(0));
        } catch (NumberFormatException nfe) {
            throw new BadRequestResponse("Unable to parse employeeID: "+ ctx.formParamMap().get("employeeID").get(0) + " into an int.");
        }
        try {
            roleID = Integer.parseInt(ctx.formParamMap().get("roleID").get(0));
        } catch (NumberFormatException nfe) {
            throw new BadRequestResponse("Unable to parse roleID: "+ ctx.formParamMap().get("roleID").get(0) + " into an int.");
        }
        if(userService.updateUser(userID,employeeID,roleID)) {
            UserAccount user = userService.getUserAccount(userID);
            ctx.json(user);
        }
    }


//    public void handleGetUserRequest(Context ctx) {
//        Map<String, List<String>> qMap;
//        int qUserID = 0;
//        int qPrivilege = 0;
//        int qEmployeeID = 0;
//        if (ctx.queryParamMap().size() == 0) {
//            if (authController.hasAdminAccess(ctx)) {
//                logger.info("UserController.handleGetUserRequest: Retrieving all user accounts for admin user: " + authController.getUsernameFromToken(ctx));
//                ctx.json(userService.getAllUserAccounts());
//            }
//            else if (authController.hasUserAccess(ctx)) {
//                String username = authController.getUsernameFromToken(ctx);
//                logger.info("UserController.handleGetUserRequest: Retrieving user account for user: " + username);
//                if (username != null)
//                    ctx.json(userService.getUserAccount(username));
//            }
//            else
//                throw new UnauthorizedResponse("You are not authorized to access this information");
//        }
//        else if (ctx.queryParamMap().containsKey("userid") || ctx.queryParamMap().containsKey("privilege") || ctx.queryParamMap().containsKey("employee-id")) {
//            if (!authController.hasUserAccess(ctx))
//                throw new UnauthorizedResponse("Only administrators can query all user accounts.");
//            if (ctx.queryParamMap().containsKey("userid"))
//                try {
//                    qUserID = Integer.parseInt(ctx.queryParam("userid"));
//                } catch (NumberFormatException e) {
//                    logger.error("UserController.handleGetUserRequest: Could not parse userid: " + ctx.queryParam(
//                            "userid") + " into an integer.");
//                }
//
//            if (ctx.queryParamMap().containsKey("privilege")) {
//                try {
//                    qPrivilege = Integer.parseInt(ctx.queryParam("privilege"));
//                } catch (NumberFormatException e) {
//                    logger.error("UserController.handleGetUserRequest: Could not parse privilege: " + ctx.queryParam(
//                            "privilege") + " into an integer.");
//                }
//            }
//            if (ctx.queryParamMap().containsKey("employee-id")) {
//                try {
//                    qEmployeeID = Integer.parseInt(ctx.queryParam("employee-id"));
//                } catch (NumberFormatException e) {
//                logger.error("UserController.handleGetUserRequest: Could not parse employee-id: " + ctx.queryParam(
//                        "employee-id") +
//                        " into an integer.");
//                }
//            }
//            logger.info("UserController.handleGetUserRequest: Retrieving filtered user accounts for admin user: " + authController.getUsernameFromToken(ctx));
//            ctx.json(userService.getFilteredUserAccounts(qUserID, qPrivilege, qEmployeeID));
//        }
//    }
//
//    public void handlePostUserRequest(Context ctx) {
//        if (!authController.hasAdminAccess(ctx))
//            throw new UnauthorizedResponse("Only administrators are allowed to link user accounts to employees");
//        if (!ctx.formParamMap().containsKey("userid") || !ctx.formParamMap().containsKey("employee-id"))
//            throw new BadRequestResponse("You must supply valid values for the userid and employee-id fields");
//        int userID = 0;
//        int employeeID = 0;
//        try {
//            userID = Integer.parseInt(ctx.formParam("userid"));
//        } catch(NumberFormatException e) {
//            logger.error("UserController.handlePostUserRequest: Could not parse userid: " + ctx.formParam(
//                    "userid") + " into an integer.");
//        }
//        try {
//            employeeID = Integer.parseInt(ctx.formParam("employee-id"));
//        } catch(NumberFormatException e) {
//            logger.error("UserController.handlePostUserRequest: Could not parse employee-id: " + ctx.formParam(
//                    "employee-id") + " into an integer.");
//        }
//        if (userID < 1 || employeeID < 1)
//            throw new BadRequestResponse("You must supply valid values for the userid and employee-id fields");
//        logger.info("UserController.handlePostUserRequest: Attempting to link userid: " + userID + " to employee-id: " + employeeID);
//        if(userService.linkUserAndEmployee(userID, employeeID))
//            ctx.result("User and Employee linked successfully");
//        else {
//            ctx.status(500);
//            ctx.result("There was a problem linking the provided userid to employee-id");
//        }
//    }
//
//    public void handleTestPull(Context ctx){
//        UserDAO uDAO = new UserDAOImpl();
//        List<String> usernames = uDAO.getAllUsernames();
//        ctx.json(usernames);
//    }
//
//    public void hashAllPasswords() {
//        userService.hashAllPasswords();
//    }
}
