package dev.kkanit.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="role")
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="role_id")
    private int roleID;
    @Column(name="role_name")
    private String roleName;
    @Column(name="has_user_access")
    private boolean hasUserAccess;
    @Column(name="has_hr_access")
    private boolean hasHRAccess;
    @Column(name="has_admin_access")
    private boolean hasAdminAccess;

    public Role(){}
    public Role(String roleName, boolean hasUserAccess, boolean hasHRAccess, boolean hasAdminAccess) {
        this.roleName = roleName;
        this.hasUserAccess = hasUserAccess;
        this.hasHRAccess = hasHRAccess;
        this.hasAdminAccess = hasAdminAccess;
    }
    public Role(int roleID, String roleName, boolean hasUserAccess, boolean hasHRAccess, boolean hasAdminAccess) {
        this.roleID = roleID;
        this.roleName = roleName;
        this.hasUserAccess = hasUserAccess;
        this.hasHRAccess = hasHRAccess;
        this.hasAdminAccess = hasAdminAccess;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean isHasUserAccess() {
        return hasUserAccess;
    }

    public void setHasUserAccess(boolean hasUserAccess) {
        this.hasUserAccess = hasUserAccess;
    }

    public boolean isHasHRAccess() {
        return hasHRAccess;
    }

    public void setHasHRAccess(boolean hasHRAccess) {
        this.hasHRAccess = hasHRAccess;
    }

    public boolean isHasAdminAccess() {
        return hasAdminAccess;
    }

    public void setHasAdminAccess(boolean hasAdminAccess) {
        this.hasAdminAccess = hasAdminAccess;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return roleID == role.roleID && hasUserAccess == role.hasUserAccess && hasHRAccess == role.hasHRAccess && hasAdminAccess == role.hasAdminAccess && Objects.equals(roleName, role.roleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleID, roleName, hasUserAccess, hasHRAccess, hasAdminAccess);
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleID=" + roleID +
                ", roleName='" + roleName + '\'' +
                ", hasUserAccess=" + hasUserAccess +
                ", hasHRAccess=" + hasHRAccess +
                ", hasAdminAccess=" + hasAdminAccess +
                '}';
    }

    public boolean identical(Role role){
        return roleName == role.roleName && hasUserAccess == role.hasUserAccess && hasHRAccess == role.hasHRAccess && hasAdminAccess == role.hasAdminAccess;
    }
}
