package dev.kkanit.utilities;

import dev.kkanit.models.Employee;
import dev.kkanit.models.Role;
import dev.kkanit.models.UserAccount;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration= new Configuration();
            Properties properties = new Properties();
            properties.put(Environment.URL,System.getenv("P1DBURL"));
            properties.put(Environment.USER,System.getenv("P1DBUSER"));
            properties.put(Environment.PASS,System.getenv("P1DBPASS"));
            properties.put(Environment.DRIVER,"com.microsoft.sqlserver.jdbc.SQLServerDriver");
            properties.put(Environment.DIALECT,"org.hibernate.dialect.SQLServerDialect");
            properties.put(Environment.HBM2DDL_AUTO,"update");
//            properties.put(Environment.HBM2DDL_AUTO,"create");
            properties.put(Environment.SHOW_SQL,"true");

            configuration.setProperties(properties);

            configuration.addAnnotatedClass(Role.class);
            configuration.addAnnotatedClass(Employee.class);
            configuration.addAnnotatedClass(UserAccount.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){ return getSessionFactory().openSession(); }
}
