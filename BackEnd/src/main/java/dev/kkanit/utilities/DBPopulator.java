package dev.kkanit.utilities;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import dev.kkanit.dao.*;
import dev.kkanit.models.Role;
import dev.kkanit.services.AuthService;
import io.javalin.http.Context;

import java.io.FileReader;
import java.io.IOException;

public class DBPopulator {

    private final static RoleDAO rDAO = new RoleDAOImpl();
    private final static EmployeeDAO eDAO = new EmployeeDAOImpl();
    private final static UserDAO uDAO = new UserDAOImpl();
    private final static AuthService authService = new AuthService();

    public static void resetDB(Context ctx) {
        rDAO.addRole(new Role("User",true,false,false));
        rDAO.addRole(new Role("HR Manager",true,true,false));
        rDAO.addRole(new Role("System Administrator",true,false,true));
        try (CSVReader csvReader =
                     new CSVReader(new FileReader("D:/Revature/Projects/Project_0/SQLFiles/employees.csv"));) {
            String[] values = null;
            String firstName;
            String lastName;
            String hireDate;
            String terminationDate;
            boolean active;
            double payRate;
            double accruedPTO;
            String federalID;
            while ((values = csvReader.readNext()) != null) {
                firstName = values[0];
                lastName = values[1];
                hireDate = values[2];
                terminationDate = values[3];
                active = Integer.parseInt(values[4]) > 0 ? true : false;
                payRate = Double.parseDouble(values[5]);
                accruedPTO = Double.parseDouble(values[6]);
                federalID = values[7];
                eDAO.populateDB(firstName,lastName,hireDate,terminationDate,active,payRate,accruedPTO,federalID);
            }
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (CSVReader csvReader =
                     new CSVReader(new FileReader("D:/Revature/Projects/Project_0/SQLFiles/userAccounts.csv"));) {
            String[] values = null;
            String username;
            String password;
            int roleID;
            String email;
            int employeeID;
            while ((values = csvReader.readNext()) != null) {
                username = values[0];
                password = authService.hashPassword(values[1]);
                roleID = Integer.parseInt(values[2]);
                email = values[3];
                employeeID = Integer.parseInt(values[4]);
                uDAO.populateDB(username,password,roleID,email,employeeID);
            }
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void resetDB() {
        rDAO.addRole(new Role("User",true,false,false));
        rDAO.addRole(new Role("HR Manager",true,true,false));
        rDAO.addRole(new Role("System Administrator",true,false,true));
        try (CSVReader csvReader =
                     new CSVReader(new FileReader("D:/Revature/Projects/Project_0/SQLFiles/employees.csv"));) {
            String[] values = null;
            String firstName;
            String lastName;
            String hireDate;
            String terminationDate;
            boolean active;
            double payRate;
            double accruedPTO;
            String federalID;
            while ((values = csvReader.readNext()) != null) {
                firstName = values[0];
                lastName = values[1];
                hireDate = values[2];
                terminationDate = values[3];
                active = Integer.parseInt(values[4]) > 0 ? true : false;
                payRate = Double.parseDouble(values[5]);
                accruedPTO = Double.parseDouble(values[6]);
                federalID = values[7];
                eDAO.populateDB(firstName,lastName,hireDate,terminationDate,active,payRate,accruedPTO,federalID);
            }
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (CSVReader csvReader =
                     new CSVReader(new FileReader("D:/Revature/Projects/Project_0/SQLFiles/userAccounts.csv"));) {
            String[] values = null;
            String username;
            String password;
            int roleID;
            String email;
            int employeeID;
            while ((values = csvReader.readNext()) != null) {
                username = values[0];
                password = authService.hashPassword(values[1]);
                roleID = Integer.parseInt(values[2]);
                email = values[3];
                employeeID = Integer.parseInt(values[4]);
                uDAO.populateDB(username,password,roleID,email,employeeID);
            }
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
