package dev.kkanit.services;

import dev.kkanit.dao.UserDAO;
import dev.kkanit.dao.UserDAOImpl;
import dev.kkanit.models.UserAccount;
import io.javalin.http.BadRequestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private AuthService authService = new AuthService();
    private UserDAO uDAO = new UserDAOImpl();

    public UserAccount registerNewUser(String username, String password, String email) throws BadRequestResponse {
        List<String> usernames = uDAO.getAllUsernames();
        if(usernames.contains(username))
            throw new BadRequestResponse("A user with that username already exists. Please try a unique username, or " +
                    "if this is your account, reset your password.");
        List<String> emails = uDAO.getAllEmails();
        if(emails.contains(email))
            throw new BadRequestResponse("An account is already registered to that e-mail address. Please double " +
                    "check your e-mail address or reset your password.");
        String hashedPassword = authService.hashPassword(password);
        return uDAO.addNewUser(username, hashedPassword, email);
    }

    public boolean changeUserPassword(String username, String password){
        int userID = uDAO.getUserAccount(username).getUserID();
        String hashedPassword = authService.hashPassword(password);
        return uDAO.changeUserPassword(userID, hashedPassword);
    }

    public int getUserID(String username) {
        if(uDAO.getUserAccount(username)!=null)
            return uDAO.getUserAccount(username).getUserID();
        return 0;
    }

    public int getEmployeeID(String username) {
        return uDAO.getEmployeeID(username);
    }

    public List<UserAccount> getAllUserAccounts () {
        logger.info("Retrieving all user accounts");
        return uDAO.getAllUserAccounts();
    }

    public boolean updateUser(int userID, int employeeID, int roleID){
        UserAccount user = uDAO.getUserAccount(userID);
        user.setRoleID(roleID);
        user.setEmployeeID(employeeID);
        return uDAO.updateUser(user);
    }

//    public List<UserAccount> getFilteredUserAccounts(int qUserID, int qPrivilege, int qEmployeeID) {
//        Map<Integer, UserAccount> userMap = new HashMap<>();
//        if (qUserID==0 && qPrivilege==0 && qEmployeeID==0)
//            return userMap;
//        int qCount = 0;
//        List<Integer> ids = new ArrayList<>();
//        StringBuilder sqlBuilder = new StringBuilder();
//        sqlBuilder.append("SELECT * FROM user_account WHERE ");
//        if (qUserID != 0) {
//            qCount++;
//            sqlBuilder.append("userid = ?");
//            ids.add(qUserID);
//        }
//        if (qPrivilege != 0) {
//            if (qCount > 0)
//                sqlBuilder.append(" and ");
//            qCount++;
//            sqlBuilder.append("privilege = ?");
//            ids.add(qPrivilege);
//        }
//        if (qEmployeeID != 0) {
//            if (qCount > 0)
//                sqlBuilder.append(" and ");
//            qCount++;
//            sqlBuilder.append("employee_id = ?");
//            ids.add(qEmployeeID);
//        }
//        String sql = sqlBuilder.toString();
//        logger.info("UserService.getFilteredUserAccounts executing SQL Statement: " + sql);
////        return uDAO.getFilteredUserAccounts(sql, ids);
//    }

    public UserAccount getUserAccount(String username) {
        logger.info("Retrieving user account for " + username);
        return uDAO.getUserAccount(username);
    }

    public UserAccount getUserAccount(int userID) {
        return uDAO.getUserAccount(userID);
    }

//    public boolean linkUserAndEmployee(int userID, int employeeID) {
////        return uDAO.updateUserEmployeeID(userID, employeeID);
//    }

//    public void hashAllPasswords() {
//        List<String> usernames = uDAO.getAllUsernames();
//        for (String user: usernames) {
//            String password = uDAO.getUserPassword(user);
//            uDAO.changeUserPassword(user, password);
//        }
//    }

    // My original hashing implementation using SHA-512
//    public static int authenticate(String username, String password) {
//        logger.info("Attempting to authenticate user.");
//        SecureRandom r = new SecureRandom();
//        byte[] salt = new byte[16];
//        int privilege = 0;
//
//        List<String> usernames = uDAO.getAllUsernames();
//        if (!usernames.contains(username)) {
//            logger.info("Unmatched username: " + username + " supplied during login");
//            return 0;
//        }
//        byte[] dbPassword = uDAO.getUserPassword(username);
//        if (dbPassword==null) {
//            logger.warn("No password retrieved.");
//            return 0;
//        }
//        r.nextBytes(salt);
//        try {
//            MessageDigest md = MessageDigest.getInstance("SHA-512");
//            md.update(salt);
//            byte[] hPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
//        } catch (NoSuchAlgorithmException e) {
//            logger.error("Hashing algorithm was not found. Authentication attempt aborted");
//            e.printStackTrace();
//            return 0;
//        }
//        privilege = uDAO.getPrivilege(username);
//        return privilege;
//    }



}
