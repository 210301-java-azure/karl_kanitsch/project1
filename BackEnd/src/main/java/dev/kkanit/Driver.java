package dev.kkanit;

import dev.kkanit.utilities.DBPopulator;

public class Driver {

    static JavalinApp app = new JavalinApp();

    public static void main(String[] args) {
//        DBPopulator.resetDB();
        app.start();
    }

}
