## Dependencies  
[Javalin](https://javalin.io/)


## Fields  

|Access|Static|Final|DataType|Name|
|---|---|---|---|---|
|private|<center>F</center>|<center>F</center>|String|username|
|private|<center>F</center>|<center>F</center>|String|password|
|private|<center>F</center>|<center>F</center>|String|email|
|private|<center>F</center>|<center>F</center>|[UserService](./docs/UserService.md)|userService|
  
## Methods  

|Access|Static|Signature|Return|Summary|  
|---|---|---|---|---| 
|public|<center>F</center>|handleNewUser(Context ctx)|void|Parses registration form data, adding a new user or throwing a Javalin Default BadRequestResponse explaining the error.|
|public|<center>F</center>|handleChangePassword(Context ctx)|void|Calls on UserService.changeUserPassword to attempt to change the users password, throwing a Javalin Default BadRequestResponse if the attempt fails.|
|public|<center>F</center>|hashAllPasswords()|void|Calls UserService.hashAllPasswords method.


[readme](../readme.md)