## Dependencies  
[SL4J Logger](http://www.slf4j.org/)  

## Fields  

|Access|Static|Final|DataType|Name|
|---|---|---|---|---|
|private|<center>F</center>|<center>F</center>|[RoleDAO](./docs/RoleDAO)|rDAO|
|private|<center>F</center>|<center>T</center>|SL4J Logger|logger|  
  
## Methods  

|Access|Static|Signature|Return|Summary|  
|---|---|---|---|---| 
|public|<center>F</center>|hasHRAccess(int privilege)|boolean|Checks the privilege against the database to and returns whether the user has HR Access privileges|
|public|<center>F</center>|hasUserAccess(int privilege)|boolean|Checks the privilege against the database to and returns whether the user has User Access privileges|
|public|<center>F</center>|hasAdminAccess(int privilege)|boolean|Checks the privilege against the database to and returns whether the user has Admin Access privileges|


[readme](../readme.md)



