## Dependencies  
[SL4J Logger](http://www.slf4j.org/)  
[Javalin](https://javalin.io/)
[Jackson](https://github.com/FasterXML/jackson)  


## Fields  

|Access|Static|Final|DataType|Name|
|---|---|---|---|---|
|private|<center>F</center>|<center>F</center>|[AuthController](./docs/AuthController.md)|authController|
|private|<center>F</center>|<center>F</center>|[EmployeeService](./docs/EmployeeService.md)|employeeService|
|private|<center>F</center>|<center>F</center>|SL4J Logger|logger|
|private|<center>F</center>|<center>F</center>|Jackson ObjectMapper|mapper|  
  
## Methods  

|Access|Static|Signature|Return|Summary|  
|---|---|---|---|---| 
|public|<center>F</center>|handleGetRequest(Context ctx)|void|Adds information to Context body depending on authorization. HR Access privileges return a list of all employees, User Access privileges return the user's information.|
|public|<center>F</center>|handleGetEmployeeRequest(Context ctx)|void|If requester has User Access privileges and is requesting their own information, it is added to Context body as a JSON. If requester has HR Access privileges and the Employee ID provided is valid, that employees information is added to Context body as a JSON.|
|public|<center>F</center>|handlePutEmployeeRequest(Context ctx)|void|If requester has HR Access privileges, attempts to add a new employee record to the database.|
|public|<center>F</center>|handleDeleteEmployeeRequest(Context ctx)|void|If requester has HR Access privileges, attempts to delete an existing employee record from the database.|


[readme](../readme.md)